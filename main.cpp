extern "C"{
#include "tibemsUtilities.h"
}
#include <string>

tibems_bool                     useTopic     = TIBEMS_TRUE;
tibems_bool                     useAsync      = TIBEMS_FALSE;
tibemsAcknowledgeMode           ackMode      = TIBEMS_AUTO_ACKNOWLEDGE;

tibemsConnectionFactory         factory      = NULL;
tibemsConnection                connection   = NULL;
tibemsSession                   session      = NULL;
tibemsMsgConsumer               msgConsumer  = NULL;

tibemsMsgProducer          		msgProducer  = NULL;
tibemsDestination               destination  = NULL;
tibemsSSLParams                 sslParams    = NULL;
tibems_int                      receive      = 1;
tibemsErrorContext              errorContext = NULL;

void onException(tibemsConnection conn,tibems_status reason,void*  closure)
{
    if (reason == TIBEMS_SERVER_NOT_CONNECTED)
        printf(" CONNECTION EXCEPTION: Server Disconnected\n");
}

int main(){
	tibems_status               status      = TIBEMS_OK;
    const char*                 txt         = NULL;
    tibemsMsgType               msgType     = TIBEMS_MESSAGE_UNKNOWN;
    std::string                 msgTypeName = "UNKNOWN";
    tibemsMsg                   msg         = NULL;
	
	
    factory = tibemsConnectionFactory_Create();
	status = tibemsConnectionFactory_SetServerURL(factory,"itp035");
	status = tibemsConnectionFactory_CreateConnection(factory,&connection,"admin","admin"); 
	status = tibemsConnection_SetExceptionListener(connection, onException, NULL);
	status = tibemsTopic_Create(&destination,"InChat.Rooms.Public.Main" );
	status = tibemsConnection_CreateSession(connection,&session,TIBEMS_FALSE,ackMode);
	status = tibemsSession_CreateConsumer(session,&msgConsumer,destination,NULL,TIBEMS_FALSE);
	status = tibemsSession_CreateProducer(session,&msgProducer,destination);
	status = tibemsConnection_Start(connection);
	status = tibemsTextMsg_Create(&msg);
	status = tibemsTextMsg_SetText(msg,"Hallo WERELD");
	status = tibemsMsgProducer_Send(msgProducer,msg);
	status = tibemsMsg_Destroy(msg);
	
	
	return 0;
}