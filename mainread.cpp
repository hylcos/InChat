extern "C"{
#include "tibemsUtilities.h"
}
#include <string>
#include <iostream>
#include <thread>


tibems_bool                     useTopic     = TIBEMS_TRUE;
tibems_bool                     useAsync      = TIBEMS_FALSE;
tibemsAcknowledgeMode           ackMode      = TIBEMS_AUTO_ACKNOWLEDGE;

tibemsConnectionFactory         factory      = NULL;
tibemsConnection                connection   = NULL;
tibemsSession                   session      = NULL;
tibemsMsgConsumer               msgConsumer  = NULL;

tibemsMsgProducer          		msgProducer  = NULL;
tibemsDestination               destination  = NULL;
tibemsSSLParams                 sslParams    = NULL;
tibems_int                      receive      = 1;
tibemsErrorContext              errorContext = NULL;

void onException(tibemsConnection conn,tibems_status reason,void*  closure)
{
    /* print the connection exception status */

    if (reason == TIBEMS_SERVER_NOT_CONNECTED)
    {
        printf(" CONNECTION EXCEPTION: Server Disconnected\n");
        receive = 0;
    }
}
void writeMessages(){
	tibemsMsg                   msg         = NULL;
	tibems_status               status      = TIBEMS_OK;
	while(1){
		status = tibemsTextMsg_Create(&msg);
		status = tibemsTextMsg_SetText(msg,"Hallo WERELD");
		status = tibemsMsgProducer_Send(msgProducer,msg);
		status = tibemsMsg_Destroy(msg);
	}
}
void readMessages(){
	tibems_status               status      = TIBEMS_OK;
	std::string                 msgTypeName = "UNKNOWN";
	tibemsMsg                   msg         = NULL;
	const char*                 txt         = NULL;
	tibemsMsgType               msgType     = TIBEMS_MESSAGE_UNKNOWN;
	
	while(1){
		status = tibemsMsgConsumer_Receive(msgConsumer,&msg);
		status = tibemsMsg_GetBodyType(msg,&msgType);
		 switch(msgType)
			{
				case TIBEMS_MESSAGE:
					msgTypeName = "MESSAGE";
					break;

				case TIBEMS_BYTES_MESSAGE:
					msgTypeName = "BYTES";
					break;

				case TIBEMS_OBJECT_MESSAGE:
					msgTypeName = "OBJECT";
					break;

				case TIBEMS_STREAM_MESSAGE:
					msgTypeName = "STREAM";
					break;

				case TIBEMS_MAP_MESSAGE:
					msgTypeName = "MAP";
					break;

				case TIBEMS_TEXT_MESSAGE:
					msgTypeName = "TEXT";
					break;

				default:
					msgTypeName = "UNKNOWN";
					break;
			}
		status = tibemsTextMsg_GetText(msg,&txt);
		std::cout << txt <<std::endl;
	}
}

int main(){
	tibems_status               status      = TIBEMS_OK;
    
    factory = tibemsConnectionFactory_Create();
	status = tibemsConnectionFactory_SetServerURL(factory,"itp035");
	status = tibemsConnectionFactory_CreateConnection(factory,&connection,"admin","admin"); 
	status = tibemsConnection_SetExceptionListener(connection, onException, NULL);
	status = tibemsTopic_Create(&destination,"InChat.Rooms.Public.Main" );
	status = tibemsConnection_CreateSession(connection,&session,TIBEMS_FALSE,ackMode);
	status = tibemsSession_CreateConsumer(session,&msgConsumer,destination,NULL,TIBEMS_FALSE);
	status = tibemsSession_CreateProducer(session,&msgProducer,destination);
	status = tibemsConnection_Start(connection);
	std::thread read(readMessages);
	std::thread write(writeMessages);
	
	while(1){
		
	}
	return 0;
}